import bagraphs
import prob
import phase1

#####################
#PA
#####################

def PA_theory_prob_dist(m, k_max):
	"""
	Returns a list of the probabilities of each degree
	"""
	t_prob_dist = [PA_theory_prob_k(k,m) for k in range(m, k_max + 1)]
	return(t_prob_dist)

def PA_theory_prob_k(k, m):
	"""
	Returns the theoretical probability of a chosen vertex having degree k
	"""
	p_k = (2*m*(m+1))/(k*(k+1)*(k+2))
	return(p_k)


def PA_cumulative_prob_dist(N,m):
	"""
	Returns the theoretical probability of a given vetex of having degree k<N
	"""
	return(1 - (N/2)*PA_theory_prob_k(N,m))

#####################
#RA
#####################

def RA_theory_prob_dist(m, k_max):
	"""
	Returns a list of the probabilities of each degree
	"""
	t_prob_dist = [RA_theory_prob_k(k,m) for k in range(m, k_max + 1)]
	return(t_prob_dist)

def RA_theory_prob_k(k,m):
	"""
	Returns the theoretical probability of a chosen vertex having degree k
	"""
	p_k = (1.0/(m + 1))*(m/(m + 1))**(k - m)
	return(p_k)


def RA_cumulative_prob_dist(N,m):
	"""
	Returns the theoretical probability of a given vetex of having degree k<N
	"""
	return(1 - (m/(m+1))**(N-m+1))