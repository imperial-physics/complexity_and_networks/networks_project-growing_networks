import numpy as np

def log_bin(dataset, a=1.1):
	"""
	Logatrithmic binning function
	Derived using J. Clough, "log_bin_CN_2016.py", 2015 with the help of Stephen Savage
	https://bb.imperial.ac.uk/bbcswebdav/pid-1034025-dt-content-rid-3193707_
	1/xid-3193707_1?target=blank

	Inputs:
		dataset: [Dataset] the dataset being log binned
		a:		 [Float]   bin expansion parameter 
	"""
	def geometric_mean(x):
		"""
		Returns the geometric mean of a list
		Inputs:
			x :  [int/float] list of numbers
		"""
		s = len(x)
		y = [np.log(z) for z in x]
		z = sum(y)/s
		return np.exp(z)

	k_list = dataset.k_list()
	k_counts = dataset.counts
	m = dataset.m

	bin_width = 1	#starting bin width
	bins = [m]	#start binning from m
	new_edge = m
	while new_edge <= dataset.k_max:
		last_edge = new_edge
		new_edge = last_edge + bin_width
		bins.append(new_edge)
		bin_width *= a


	probs = [0 for x in bins[1:]]	#set each bin to 0 first
	indices = np.digitize(k_list, bins[1:])	#index of bin which k_values falls

	for i,k in enumerate(k_list):	#i = k_values[i]
		j = indices[i]	#j = bin index which i belongs to
		print(j)
		probs[j] += k_counts[i]	#adds probability of i to appropriate bin

	##Calculation of centres of each bin in log space
	bin_indices = range(len(bins)-1)        
	int_in_each_bin = [range(int(np.ceil(bins[i])), \
					   int(np.ceil(bins[i+1]))) for i in bin_indices]
	centres = [geometric_mean(x) for x in int_in_each_bin]

	##Adjustment to account for different sized bins and normalise probabilities using the total number of datapoints
	widths = [np.ceil(bins[i+1]) - np.ceil(bins[i]) for i in bin_indices]
	probs /= (np.array(widths)* dataset.N * dataset.iterations)

	return  centres, probs