import main
import bagraphs
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import log_bin
from cycler import cycler
from itertools import zip_longest
import math


#########################################
## Part 3
#########################################

def plot_avg_p_fixed_N(data_ensemble, a=1.2, binned=True):
	"""
	Plots the average degree probability distribution of a number of simulations for networks of size N.
	This is done for a range of m. 

	Inputs:
		data_ensemble: [list of Datasets]: Datasets with varying m
		a: [float] bin expansion paramter
		binned: [bool] whether data is log binned or not
	"""
	
	figure, ax = plt.subplots()
	ax.set_xlabel("$k$")
	ax.set_ylabel("$p(k)$")
	ax.set_xscale("log")
	ax.set_yscale("log")
	ax.set_prop_cycle(cycler('color', ['b','b', 'g', 'g', 'r', 'r', 'm', 'm', 'y','y', 'k','k']))


	for dataset in data_ensemble:
		

		if binned == True:
			bin_centres, prob_dist = log_bin.log_bin(dataset, a=a)
			ax.plot(bin_centres, prob_dist, label="$m = %s $" % dataset.m)
		else:
			ax.plot(dataset.k_list(), np.array(dataset.counts)/(dataset.N*dataset.iterations), "o", label="$m = %s$" % dataset.m)

		ax.plot(dataset.k_list(), main.PA_theory_prob_dist(dataset.m, dataset.k_max), "--")


	ax.legend(loc='best').draggable()
	figure.show()

def plot_avg_p_fixed_m(data_ensemble, a=1.2):
	"""
	Plots the average degree probability distribution of a number of simulations for networks with a single m.
	This is done for a range of N to show finite size effects. 


	Inputs:
		data_ensemble: [list of Datasets]: Datasets with varying N
		a: [float] bin expansion paramter
	"""
	
	figure, ax = plt.subplots()
	ax.set_xlabel("$k$")
	ax.set_ylabel("$p(k)$")
	ax.set_xscale("log")
	ax.set_yscale("log")


	k_max = 0
	m = 0
	for dataset in data_ensemble:

		bin_centres, prob_dist = log_bin.log_bin(dataset, a=a)
		ax.plot(bin_centres, prob_dist, label="$L =%s$" % dataset.L)
		k_max = max(k_max, dataset.k_max)
		m = dataset.m
	
	ax.plot(range(m,k_max+1), main.PA_theory_prob_dist(m,k_max), "--", label="Theory")


	ax.legend(loc='best').draggable()
	figure.show()


#########################################
## Part 4
#########################################

def linear_fit(N, m, c):
	return(m * np.array(N) + c)

def PA_theoretical_k1(N, m):
	"""
	Calculates theoretical values of k_1 for a list of network sizes N 

	Inputs:
		N: [list of ints] System sizes for which to calculate k_1
		m: [int] Value of m used in BA_Graph_PA which this theoretical form is to be compared to
	"""
	return(-1/2 + np.sqrt(1/4 + np.array(N) * m * (m+1)))

def RA_theoretical_k1(N, m):
	"""
	Calculates theoretical values of k_1 for a list of network sizes N 

	Inputs:
		N: [list of ints] System sizes for which to calculate k_1
		m: [int] Value of m used in BA_Graph_PA which this theoretical form is to be compared to
	"""
	return(np.log(N)/(np.log(m+1)/m)+(m))

def plot_k1(data_ensemble_list):
	figure, ax = plt.subplots()
	ax.set_xlabel("$N$")
	ax.set_ylabel("$k_1$")
	ax.set_xscale("log")
	ax.set_yscale("log")
	ax.set_prop_cycle(cycler('color', ['b','b', 'g', 'g', 'r', 'r', 'm', 'm', 'y','y', 'k','k']))


	#fit = curve_fit(linear_fit, np.log(N), np.log(data))

	#print("gradient:", fit[0][0], "+-", fit[1][0,0]**0.5)
	for data_ensemble in  data_ensemble_list:
		m = data_ensemble[0].m
		N = [dataset.N for dataset in data_ensemble]
		theory = [RA_theoretical_k1(n, data_ensemble[0].m) for n in N]
		data = [dataset.E_of_k_raised_n(1) for dataset in data_ensemble]
		error = [dataset.standard_deviation() for dataset in data_ensemble]			

		ax.errorbar(N, data, yerr=error, marker="x", fmt="o", label="$m=%s$" % data_ensemble[0].m)
		ax.plot(N, theory, "--")#,label=r"Theory $m=%s$" % data_ensemble[0].m)
	ax.legend(loc='best').draggable()
	ax.set_xlim([10,2*10**6])
	figure.show()


#########################################
## Data collapse
#########################################

def plot_data_collapse(data_ensemble, a=1.1):
	"""
	Plots the average degree probability distribution of a number of simulations for networks with a single m.
	This is done for a range of N to show finite size effects. 


	Inputs:
		data_ensemble: [list of Datasets]: Datasets with varying N
		a: [float] bin expansion paramter
	"""
	
	figure, ax = plt.subplots()
	ax.set_xlabel("$k/k_1$")
	ax.set_ylabel("$p(k)/p_{\infty}(k)$")
	ax.set_xscale("log")
	ax.set_yscale("log")

	for dataset in data_ensemble:

		bin_centres, prob_dist = log_bin.log_bin(dataset, a=a)
		
		if dataset.model == "PA":
			scaled_bins = np.array(bin_centres)/PA_theoretical_k1(dataset.N,dataset.m)
			p_theory = [main.PA_theory_prob_k(k,dataset.m) for k in bin_centres]
		elif dataset.model == "RA":
			scaled_bins = np.array(bin_centres)/RA_theoretical_k1(dataset.N,dataset.m)
			p_theory = [main.RA_theory_prob_k(k,dataset.m) for k in bin_centres]
		
		scaled_prob = np.array(prob_dist)/np.array(p_theory)

		ax.plot(scaled_bins, scaled_prob, label="$N = 10^{%s}$" % int(math.log10(dataset.N)))


	ax.legend(loc='best').draggable()
	figure.show()
