main.py: various probability distribution functions
bagraphs.py: network classes, handles main algorithm work
phase1.py: Plotting of data
data_collection.py: collection of data
prob: statistical tests
log_bin.py: Improved log binning function created with help of Stephen Savage

Data can be collected and tested by running data_collection.fixed_N_data_collection(), etc.
When finished, the data will be saved to a pickle file.
This can then be unpickled and used as an argument for the relevant function in phase1.py

Direct handling of bagraph classes will require some work to convert it into the proper form for plotting. Not recommended. 