import scipy.stats.distributions
import matplotlib.pyplot as plt
import numpy as np
import main

def empirical_cdf(dataset):
	"""
	Calculates a empirical cumulative degree probability distribution from a graph
	Input:
		graph: [BA_Graph] Graph used to calculate cdf

	Output:
		cdf: [list of floats] cumulative degree probability distribution

	"""

	prob_dist = [float(x)/(dataset.N*dataset.iterations) for x in dataset.give_counts()]

	cdf = [sum(prob_dist[0:i+1]) for i, x in enumerate(prob_dist)]
	return(cdf)

def theoretical_cdf(dataset):
	"""
	Calculates the theoretical cumulative degree probability distribution from parameters of graph
	Input:
		graph: [BA_Graph] Graph with parameters used to calculate cdf

	Output:
		cdf: [list of floats] cumulative degree probability distribution

	"""
	
	cdf = [main.PA_cumulative_prob_dist(k, dataset.m) for k in dataset.k_list()]
	return(cdf)

def plot_cum_dists(dataset):
	"""
	Plots a figure showing empirical and theoretical cdfs for a given graph superimposed

	Input:
		graph: [BA_Graph] Graph used to calculate cdfs

	"""
	figure, ax = plt.subplots()
	ax.set_xlabel("$k$")
	ax.set_ylabel("$P(k)$")
	ax.set_xscale("log")

	k_list = dataset.k_list()
	ax.step(k_list, empirical_cdf(dataset), label="Empirical CDF")
	ax.step(k_list, theoretical_cdf(dataset), label="Analytical CDF")
	ax.legend(loc='best').draggable()

	figure.show()

def homebrew_ks(dataset):
	"""
	Performs a Kolmogorov-Smirnov test on a graph to determine how closely its empirical cdf follows the theoretical cdf


	Input:
		graph: [BA_Graph] Graph on which KS test is performed

	Outputs:
		D_n: [float] test statistic of KS test
		p_value: [float] p-value associated with D_n
	"""
	F_n = empirical_cdf(dataset)
	F_0 = theoretical_cdf(dataset)

	D_n = max(np.abs(np.array(F_n) - np.array(F_0)))
	p_value = scipy.stats.distributions.kstwobign.sf(D_n*len(F_n)**0.5)
	return(D_n, p_value)