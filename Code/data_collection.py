import main
import bagraphs
import numpy as np
import pickle
from itertools import zip_longest
import matplotlib.pyplot as plt

class Dataset():


	def __init__(self, N, m, iterations, model, L=None):
		self.N = N
		self.m = m
		self.iterations = iterations
		self.model = model
		if model == "RW":
			self.L = L

	def save_data(self, data):
		self.k_max = max(data)
		k_list = range(self.m, self.k_max+1)
		self.counts = [data.count(k) for k in k_list]

	def give_counts(self):
		return(self.counts)

	def give_raw_data(self):
		raw_data = []
		for i, k in enumerate(self.counts):
			raw_data += [(self.m + i)] * k
		return(raw_data)

	def k_list(self):
		return(range(self.m,self.k_max+1))

class RankDataset():

	def __init__(self, N, m, iterations, model, L=None):
		self.N = N
		self.m = m
		self.iterations = iterations
		self.model = model
		if model == "RW":
			self.L = L

	def save_data(self,data):
		self.k_max_list = data

	def E_of_k_raised_n(self, n):
		k_array = np.array(self.k_max_list)
		return(sum(k_array**n)/self.iterations)


	def standard_deviation(self):
		std_dev = (self.E_of_k_raised_n(2) - (self.E_of_k_raised_n(1))**2)**0.5
		return(std_dev)

def average_prob_data_PA(N, m, iterations):
	"""
	Simulates a network up to size N for a number of iterations


	Outputs:
	k_list: [list] range(m,k_max + 1) where k_max is the largest observed degree
	avg_degree_dist: [list] averaged degree probability distribution
	total_degrees: [list] list of degrees of every vertex in all simulations
	"""
	
	## List of the degrees of all nodes over all iterations. Required for binning
	total_degrees = []

	for rep in range(iterations):

		print("Running iteration ", rep + 1 ,"/", iterations, end="\r")
		## Initialise network and iterate to N nodes
		network = bagraphs.BA_Graph_PA(m, m)
		network.add_vertices_until_N(N)
		total_degrees += network.graph_degrees()
	print("\n")

	return(total_degrees)

def average_prob_data_RA(N, m, iterations):
	"""
	Simulates a network up to size N for a number of iterations


	Outputs:
	k_list: [list] range(m,k_max + 1) where k_max is the largest observed degree
	avg_degree_dist: [list] averaged degree probability distribution
	total_degrees: [list] list of degrees of every vertex in all simulations
	"""
	
	## List of the degrees of all nodes over all iterations. Required for binning
	total_degrees = []

	for rep in range(iterations):

		print("Running iteration ", rep + 1 ,"/", iterations, end="\r")
		## Initialise network and iterate to N nodes
		network = bagraphs.BA_Graph_RA(m, m)
		network.add_vertices_until_N(N)
		total_degrees += network.graph_degrees()
	print("\n")

	return(total_degrees)

def average_prob_data_RW(N, m, L, iterations):
	"""
	Simulates a network up to size N for a number of iterations


	Outputs:
	k_list: [list] range(m,k_max + 1) where k_max is the largest observed degree
	avg_degree_dist: [list] averaged degree probability distribution
	total_degrees: [list] list of degrees of every vertex in all simulations
	"""
	
	## List of the degrees of all nodes over all iterations. Required for binning
	total_degrees = []

	for rep in range(iterations):

		print("Running iteration ", rep + 1 ,"/", iterations, end="\r")
		## Initialise network and iterate to N nodes
		network = bagraphs.BA_Graph_RW(m, m, L)
		network.add_vertices_until_N(N)
		total_degrees += network.graph_degrees()
	print("\n")

	return(total_degrees)

def average_rank_data_PA(N, m, iterations):
	"""
	Simulates a network up to size N for a number of iterations


	Outputs:
	k_list: [list] range(m,k_max + 1) where k_max is the largest observed degree
	avg_degree_dist: [list] averaged degree probability distribution
	total_degrees: [list] list of degrees of every vertex in all simulations
	"""
	
	## List of the degrees of all nodes over all iterations. Required for binning
	k_max_list = []

	for rep in range(iterations):

		print("Running iteration ", rep + 1 ,"/", iterations, end="\r")
		## Initialise network and iterate to N nodes
		network = bagraphs.BA_Graph_PA(m, m)
		network.add_vertices_until_N(N)
		k_max_list.append(max(network.graph_degrees()))
	print("\n")

	return(k_max_list)


def average_rank_data_RA(N, m, iterations):
	"""
	Simulates a network up to size N for a number of iterations


	Outputs:
	k_list: [list] range(m,k_max + 1) where k_max is the largest observed degree
	avg_degree_dist: [list] averaged degree probability distribution
	total_degrees: [list] list of degrees of every vertex in all simulations
	"""
	
	## List of the degrees of all nodes over all iterations. Required for binning
	k_max_list = []

	for rep in range(iterations):
		print("Running iteration ", rep + 1 ,"/", iterations, end="\r")
		## Initialise network and iterate to N nodes
		network = bagraphs.BA_Graph_RA(m, m)
		network.add_vertices_until_N(N)
		k_max_list.append(max(network.graph_degrees()))
	print("\n")

	return(k_max_list)

def save_data(N,m,iterations,model, L=0):
	if model == "PA":
		degrees = average_prob_data_PA(N,m,iterations)
	elif model == "RA":
		degrees = average_prob_data_RA(N,m,iterations)
	elif model == "RW":
		degrees = average_prob_data_RW(N,m,L,iterations)
		
	data = Dataset(N,m,iterations, model, L)
	data.save_data(degrees)
	pickle.dump(data, open("%s N=%s, m=%s, L=%s, iter=%s.p" % (model,N,m,L,iterations), "wb"))

def save_rank_data(N,m,iterations,model, L=0):
	if model == "PA":
		k_max_list = average_rank_data_PA(N,m,iterations)
	elif model == "RA":
		k_max_list = average_rank_data_RA(N,m,iterations)
	elif model == "RW":
		k_max_list = average_rank_data_RW(N,m,L,iterations)
		
	data = RankDataset(N,m,iterations, model, L)
	data.save_data(k_max_list)
	pickle.dump(data, open("%s Rank N=%s, m=%s, L=%s, iter=%s.p" % (model,N,m,L,iterations), "wb"))

def fixed_m_data_collection(N=[2,3,4,5,6],m=1, model="PA", L=0):
	for n in N:
		save_data(10**n,m,2*10**(6-n), model, L)

def fixed_N_data_collection(N=10**4, M=[0,1,2,3,4,5], model="PA", L=0):
	for m in M:
		save_data(N,3**m,100*2**(6-m), model, L)


def rank_data_collection(N=[2,3,4,5,6],m=1, model="PA", L=0):
	for n in N:
		save_rank_data(10**n,m,2*10**(5-n), model, L)


def merge_data(list1, list2):
	"""
	Function to sum together the degree probabliity distributions for two repeats of the simulation.

	Note: The returned "probablity distribution" is not normalised!
		  The resulting distribution must be divded through by the number of repeats. 
	"""
	return([x + y for x, y in zip_longest(list1, list2, fillvalue=0)])