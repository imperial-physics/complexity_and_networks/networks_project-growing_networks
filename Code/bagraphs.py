import networkx as nx
import random
import matplotlib.pyplot as plt
import numpy as np

################################################
# Graph class definitions
################################################

class BA_Graph():
	"""
	Parent class holding common functions
	such as the graph initialisation and degree distribution calculation
	"""

	## Initialisation and iteration methods


	def __init__(self, m, m_0):
		"""
		Sets values for graph and initialises it
		"""
		self.m = m
		self.L = None
		self.initialise_graph(m, m_0)
		self.nodelist = self.graph.nodes()
		self.edgelist = self.graph.edges()

	def initialise_graph(self, m, m_0):
		"""
		Creates a graph with m0 nodes for subsequent nodes to connect to.
		"""
		self.graph = nx.complete_graph(m+1)


	def add_vertex(self,vertex):
		"""
		Function to add new node "vertex" to graph
		m edges are created between the new and existing nodes
		"""

		m = self.m

		## sets automatically remove duplicates
		## ensures that m distinct nodes are chosen
		linked_nodes = set()

		## Check if a random walk is occuring
		if self.L == None:
			## Adds m random nodes at a time until set is filled
			while len(linked_nodes) < m:
				linked_nodes.update([self.random_vertex()] * m)
		else:
			## Adds m random nodes at a time until set is filled
			while len(linked_nodes) < m:
				linked_nodes.update([self.random_vertex(self.L)] * m)

		## slices list to remove any excess nodes
		linked_nodes = list(linked_nodes)[:m]
				
		## Adds the new node
		self.graph.add_node(vertex)
		## Adds edges between new node and m existing nodes
		for i in linked_nodes:
			self.graph.add_edge(vertex, i)

		self.nodelist += [vertex]
		self.edgelist += [(vertex, i) for i in linked_nodes]

	## Methods to automate addition of multiple nodes

	def add_n_vertices(self,n):
		start_vertex_ref = self.graph.nodes()[-1]
		for i in range(start_vertex_ref, start_vertex_ref + n + 1):
			print(i)
			self.add_vertex(i)

	def add_vertices_until_N(self,N, print_N = False):
		while len(self.nodelist) < N:
			if print_N == True:
				print(len(self.nodelist))
			self.add_vertex(self.nodelist[-1] + 1)

	## Methods to display graph state

	def show_graph(self):
		"""
		Plots a visualisation of the graph.
		Warning: Takes a long time for large graphs
		"""
		figure, ax = plt.subplots()
		nx.draw(self.graph, ax=ax)
		figure.show()

	def print_graph_state(self):
		"""
		Prints graph state
		Used for testing for small N
		"""

	## Calculation of statistics

	def graph_degrees(self):
		return([self.graph.degree(vertex) for vertex in self.nodelist])


	def degree_distribution(self):
		"""
		Returns a list of the number of nodes with degree k for values of k between the max and min observed
		"""
		vertex_degrees = self.graph_degrees()
		k_max = max(vertex_degrees)
		k_min = min(vertex_degrees)
		k_list = range(k_min, k_max+1)
		distribution = [(k,vertex_degrees.count(k)) for k in k_list]
		return(distribution)

	def degree_prob_dist(self):
		vertex_degrees = self.graph_degrees()
		k_max = max(vertex_degrees)
		k_min = min(vertex_degrees)
		k_list = range(k_min, k_max+1)
		N = len(self.nodelist)
		distribution = [(k,float(vertex_degrees.count(k)/N)) for k in k_list]
		return(distribution)


class BA_Graph_PA(BA_Graph):
	"""
	Class for graphs constructed under "Preferential Attachment"
	"""

	def __init__(self, m, m0):
		BA_Graph.__init__(self, m, m0)
		

	def random_vertex(self):
		"""
		Returns a vertex with probability k_i/sum(k_i)
		where k_i is the degree of the i'th vertex
		"""

		random_edge = random.choice(self.edgelist)
		chosen_vertex = random.choice(random_edge)

		return(chosen_vertex)

	## Calculation of statistics

	def theory_prob_dist(self):
		"""
		Returns a list of the probabilities of each degree
		"""
		t_prob_dist = [self.theory_prob_k(k) for k in range(self.m, max(self.graph_degrees()) + 1)]
		return(t_prob_dist)

	def theory_prob_k(self, k):
		"""
		Returns the theoretical probability of a chosen vertex having degree k
		"""
		p_k = (2*self.m*(self.m+1))/(k*(k+1)*(k+2))
		return(p_k)


	def cumulative_prob_dist(self, N):
		"""
		Returns the theoretical probability of a given vetex of having degree k<N
		"""
		return(1 - (N/2)*self.theory_prob_k(N))


class BA_Graph_RA(BA_Graph):
	"""
	Class for graphs constructed under "Random Attachment"
	"""

	## Initialisation and iteration methods

	def __init__(self, m, m0):
		BA_Graph.__init__(self, m, m0)

	def random_vertex(self):
		"""
		Returns a vertex chosen with a uniform distribution
		"""
		chosen_vertex = random.choice(self.nodelist)
		return(chosen_vertex)

	## Calculation of statistics

	def theory_prob_dist(self):
		"""
		Returns a list of the probabilities of each degree
		"""
		t_prob_dist = [self.theory_prob_k(k) for k in range(self.m, max(self.graph_degrees()) + 1)]
		return(t_prob_dist)

	def theory_prob_k(self, k):
		"""
		Returns the theoretical probability of a chosen vertex having degree k
		"""
		p_k = (1.0/(self.m + 1))*(self.m/(self.m + 1))**(k - self.m)
		return(p_k)


	def cumulative_prob_dist(self, N):
		"""
		Returns the theoretical probability of a given vetex of having degree k<N
		"""
		return(1 - (N/2)*self.theory_prob_k(N))


class BA_Graph_RW(BA_Graph):
	"""
	Class for graphs constructed under "Random Walk and Preferential Attachment"
	"""

	## Initialisation and iteration methods

	def __init__(self, m, m0, L):
		BA_Graph.__init__(self, m, m0)
		self.L  = L

	def random_vertex(self, L):
		"""
		Returns a vertex chosen with a uniform distribution
		followed by a random walk of L steps.

		For L=0, BA_Graph_RW reduces to BA_Graph_RA
		"""
		chosen_vertex = random.choice(self.nodelist)

		step = 0
		while step < L:
			chosen_vertex = self.random_walk(chosen_vertex)
			step += 1

		return(chosen_vertex)

	def random_walk(self, vertex):
		"""
		Chooses a random neighbour of the given vertex and returns it.
		"""
		neighbour = random.choice(self.graph.neighbors(vertex))
		return(neighbour)